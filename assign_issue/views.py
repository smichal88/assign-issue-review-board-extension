# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, redirect
from reviewboard.reviews.models import Comment


def comment(request, comment_id):
    """assign to comment"""
    com = get_object_or_404(Comment, pk=comment_id)
    assigned = request.GET.get("assigned")
    com.extra_data["assigned"] = assigned
    com.save()

    return redirect(com.get_review_url())
