from __future__ import unicode_literals

from django.conf.urls import patterns, url

from assign_issue.extension import AssignIssue


urlpatterns = patterns(
    'assign_issue.views',

    url(r'^$', 'configure'),
)