# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from assign_issue import views

urlpatterns = patterns('assign_issue.views',
                       url(r'^(?P<comment_id>[0-9]+)/$',
                           views.comment,
                           name="assign-issue-comment"))
