# assign_issue Extension for Review Board.

from __future__ import unicode_literals

from django.conf.urls import patterns, include
from django.core.urlresolvers import reverse
from reviewboard.extensions.base import Extension
from reviewboard.extensions.hooks import CommentDetailDisplayHook, URLHook

from reviewboard.urls import review_request_url_names, reviewable_url_names


apply_to_url_names = set(reviewable_url_names + review_request_url_names)


class AssignedIssueDetailDisplay(CommentDetailDisplayHook):
    LABEL = "Assigned User"

    def render_review_comment_detail(self, comment):

        if not comment.issue_opened:
            return ""
        assigned = comment.extra_data.get("assigned")

        if not assigned:
            assigned = ""

        if comment.issue_status == 'O':
            assing_url = reverse("assign-issue-comment", args=(comment.id,))
            return ('<form action="{0}" method="get" accept-charset="utf-8">'
                    '<label for="assigned_id_{1}">Assigned:</label>'
                    '<input id="assigned_id_{1}" type="text" name="assigned" value={2}></input>'
                    '<input type="submit" value="Assign&rarr;">'
                    '</form>'.format(assing_url,
                                     comment.id,
                                     assigned))
        if not assigned:
            return ""
        return ('<p>'
                '<label for="assigned_id_{0}">Assigned:</label>'
                '<span id="assigned_id_{0}">{1}</span>'

                '</p>'.format(comment.id,
                              assigned))

    def render_email_comment_detail(self, comment, is_html):

        assigned = comment.extra_data.get("assigned")

        if not assigned:
            return ""
        if is_html:
            return ('<p>'
                    '<span>Assigned:</span>'
                    '<span>{0}</span>'.format(assigned))
        return 'Assigned: {0}'


class AssignIssue(Extension):
    metadata = {
        'Name': 'assign_issue',
        'Summary': 'Describe your extension here.',
    }

    def __init__(self, *args, **kwargs):
        super(AssignIssue, self).__init__(*args, **kwargs)
        pattern = patterns('', (r'^assign_issue/',
                                include('assign_issue.urls')))
        self.url_hook = URLHook(self, pattern)

    def initialize(self):
        AssignedIssueDetailDisplay(self)
