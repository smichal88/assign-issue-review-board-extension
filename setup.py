#!/usr/bin/env python
from __future__ import unicode_literals

from reviewboard.extensions.packaging import setup


PACKAGE = "assign_issue"

setup(
    name=PACKAGE,
    version="0.1.0",
    description="Extension assign_issue",
    author="sobczakm",
    packages=['assign_issue', ],
    entry_points={
        'reviewboard.extensions': [
            PACKAGE + ' = assign_issue.extension:AssignIssue',
        ],
    },
)
